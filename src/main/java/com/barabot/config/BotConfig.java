package com.barabot.config;

/**
 *
 * @author Renan
 */
public class BotConfig {
    private String host;
    private String username;
    private String password;
    private int virtualServerId;
    private String serverName;
    private String botName;

    public String getBotName() {
        return botName;
    }

    public void setBotName(String botName) {
        this.botName = botName;
    }
    
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getVirtualServerId() {
        return virtualServerId;
    }

    public void setVirtualServerId(int virtualServerId) {
        this.virtualServerId = virtualServerId;
    }
    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
    
}
