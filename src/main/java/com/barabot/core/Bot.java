/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.barabot.core;

import com.barabot.config.BotConfig;
import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.VirtualServerProperty;
import java.util.HashMap;
import java.util.logging.Level;

/**
 *
 * @author Renan
 */
public class Bot {
    private BotConfig config;
    private TS3Query query;
    private TS3Api api;
    
    public TS3Query getQuery() {
        return query;
    }

    public void setQuery(TS3Query query) {
        this.query = query;
    }

    public TS3Api getApi() {
        return api;
    }

    public void setApi(TS3Api api) {
        this.api = api;
    }
        
    public BotConfig getConfig() {
        return config;
    }

    public void setConfig(BotConfig config) {
        this.config = config;
    }
    
    public Bot(BotConfig config)
    {
        this.config = config;
    }
    
    public void start()
    {
        connectToServer();
        login();
        setupDefaults();
        
    }
    
    private void setupDefaults()
    {
        final HashMap<VirtualServerProperty, String> properties = new HashMap<>();
        properties.put(VirtualServerProperty.VIRTUALSERVER_NAME, this.config.getServerName());
        api.editServer(properties);
    }
    
    private void login()
    {
        api.login(this.config.getUsername(), this.config.getPassword());
        api.selectVirtualServerById(this.config.getVirtualServerId());
        api.setNickname(this.config.getBotName());
        api.sendChannelMessage(String.format("%s is online!", this.config.getBotName()));
    }
    
    private void connectToServer()
    {
        final TS3Config tsConfig = new TS3Config();
        tsConfig.setHost(this.config.getHost());
        tsConfig.setDebugLevel(Level.ALL);

        setQuery(new TS3Query(tsConfig));
        query.connect();
        
        setApi(query.getApi());
    }
}
